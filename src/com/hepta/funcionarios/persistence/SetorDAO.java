package com.hepta.funcionarios.persistence;

import javax.persistence.EntityManager;

import com.hepta.funcionarios.entity.Setor;

public class SetorDAO {
	public Setor find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Setor setor = null;
		try {
			setor = em.find(Setor.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return setor;
	}
}
