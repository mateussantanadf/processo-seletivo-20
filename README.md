processo-seletivo
O teste foi feito com uma arquitetura bem simples para facilitar o entendimento. É um pequeno sistema para gerenciar os funcionários dos setores.
É possível exibir todos os funcionários existentes se os dados forem inseridos manualmente na base de dados.
É preciso que você implemente as funcionalidades de inserir, editar e
deletar um funcionário.
Foi implementado apenas as classes relacionadas a entidade Funcionario.

Requisitos

Continuar o desenvolvimento do sistema
Utilizar no Front-end HTML, CSS e Javascript (Foi implementado utilizando Vue e Axios)
Web Services RESTful em Java usando Jersey
Integração com Banco de Dados
Testes de integração


O que fazer agora
Faça um clone desse projeto e divida em commits os passos necessários para a
produção do resultado final. Quando terminar envie o link do projeto no seu repositório para gustavo.oliveira@hepta.com.br com o título "Processo seletivo - [seu nome]", se você nunca usou git crie uma conta no gitlab e
dê uma olhada nos links abaixo.
Lembre-se de adicionar um arquivo HOWTO.md descrevendo o necessário para execução
da aplicação e dos testes.
Trabalhamos com várias tecnologias porém a maioria dos sistemas são em Java no back-end e Vue.js no front-end, JUnit para os testes de integração.

Avaliação
O objetivo desse teste é medir o seu conhecimento sobre as boas práticas de programação, facilidade em aprender novas tecnologias e de melhorar/continuar projetos em andamento.
Você tem o prazo de 1 semana, a partir do dia posterior ao de envio.
Mesmo que não complete todo o teste, envie mesmo assim,
ele não é de caráter desclassificatório, mas sim, classificatório.
Sinta-se a vontade para implementar mais funcionalidades e alterações de interface.

Links úteis


Git

git - guia prático
git - documentacao



Web services Java e Jersey

WebService



HTML/CSS e Javascript

Tudo sobre Javascript, HTML e CSS
HTML
CSS
Javascript
bootstrap
W3 Bootstrap

Vue JS - Uma lib JS baseada em Angular porém mais simples.

Axio - Lib JS recomendada para Vue JS para simplificar XMLHttpRequests.